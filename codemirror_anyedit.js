/*
Copyright (C) 2012 by Kevin Walton.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


var CodeMirrorAnyEditor = function( txtarea ){
var anyeditor = {};

//  '<div style="border: 1px solid #BBBBBB; background-color: #EEEEEE">'+
var src = ''+
  '<input type="text" size="5" maxlength="4" placeholder="line" id="linenum" />'+
    '<select id="mode" style="margin-left:20px;">'+
    '<option value="text/plain" selected="selected">- Select language -</option>'+
    '<optgroup label="Web Client">'+
      '<option value="text/javascript">JavaScript</option>'+
      '<option value="application/json">JSON</option>'+
      '<option value="text/html">HTML</option>'+
      '<option value="text/css">CSS</option>'+
      '<option value="text/x-less">CSS-LESS</option>'+
    '</optgroup>'+
    '<optgroup label="Scripting">'+
      '<option value="text/x-python">Python</option>'+
      '<option value="text/x-python">Python 3</option>'+
      '<option value="text/x-php">PHP</option>'+
      '<option value="text/x-perl">Perl</option>'+
      '<option value="text/x-lua">Lua</option>'+
      '<option value="text/x-ruby">Ruby</option>'+
      '<option value="text/vbscript">VBScript</option>'+
      '<option value="text/x-coffeescript">CoffeeScript</option>'+
    '</optgroup>'+
    '<optgroup label="Server Scripting">'+
      '<option value="application/x-httpd-php">PHP (with HTML)</option>'+
      '<option value="application/x-jsp">JSP</option>'+
      '<option value="application/x-aspx">ASP</option>'+
    '</optgroup>'+
    '<optgroup label="Programming">'+
      '<option value="text/x-java">Java</option>'+
      '<option value="text/x-groovy">Groovy</option>'+
      '<option value="text/x-scala">Scala</option>'+
      '<option value="text/x-erlang">Erlang</option>'+
      '<option value="text/x-csharp">C#</option>'+
      '<option value="text/x-c++src">C++</option>'+
      '<option value="text/x-csrc">C</option>'+
    '</optgroup>'+
    '<optgroup label="Database">'+
      '<option value="text/x-mysql">SQL</option>'+
      '<option value="text/x-plsql">PL/SQL</option>'+
    '</optgroup>'+
    '<optgroup label="Markup">'+
      '<option value="application/xml">XML</option>'+
      '<option value="text/x-rst">reStructured Text</option>'+
      '<option value="text/x-markdown">Markdown</option>'+
      '<option value="text/x-tiddlywiki">TiddlyWiki</option>'+
      '<option value="tiki">TikiWiki</option>'+
      '<option value="text/x-stex">LaTeX</option>'+
      '<option value="text/x-yaml">YAML</option>'+
    '</optgroup>'+
    '<optgroup label="Template">'+
      '<option value="text/x-smarty">Smarty</option>'+
      '<option value="text/velocity">Velocity</option>'+
      '<option value="application/xquery">XQuery</option>'+
      '<option value="jinja2">Jinja</option>'+
    '</optgroup>'+
    '<optgroup label="OS">'+
      '<option value="text/x-sh">BASH / Shell</option>'+
      '<option value="text/x-diff">diff / patch</option>'+
      '<option value="text/x-ini">ini</option>'+
      '<option value="text/x-rpm-changes">changelog</option>'+
      '<option value="text/x-rpm-spec">rpm spec</option>'+
    '</optgroup>'+
    '<optgroup label="More Programming">'+
      '<option value="text/x-vb">VB.NET</option>'+
      '<option value="text/x-clojure">Clojure</option>'+
      '<option value="text/x-haskell">Haskell</option>'+
      '<option value="text/x-pascal">Pascal</option>'+
      '<option value="text/x-scheme">Scheme</option>'+
      '<option value="text/x-stsrc">Smalltalk</option>'+
      '<option value="text/x-go">Go</option>'+
      '<option value="text/x-rsrc">R</option>'+
      '<option value="text/x-haxe">Haxe</option>'+
      '<option value="application/sieve">Sieve</option>'+
      '<option value="text/x-ecl">ECL</option>'+
      '<option value="text/x-rustsrc">Rust</option>'+
    '</optgroup>'+
  '</select>'+

  '<!--'+
  '<span style="margin-left: 20px; padding:0">'+
  '<img src="images/selection-select.png" />'+
  '<img src="images/scissors-blue.png" />'+
  '<img src="images/document-copy.png" />'+
  '<img src="images/clipboard-paste.png" />'+
  '</span>'+
  '-->'+
  '<span style="margin-left: 20px; padding:0">'+
  '<img src="images/arrow-curve-180-left.png" alt="Undo" id="undobtn" />'+
  '<img src="images/arrow-curve.png" alt="Redo" id="redobtn" />'+
  '</span>'+
  '<span style="margin-left: 20px; padding:0">'+
  '<img src="images/edit-indent.png" alt="Format Code" id="formatbtn" />'+
  '</span>'+
  '<span style="margin-left: 20px; padding:0">'+
  '<img src="images/keyboard.png" />'+
  '<select id="keymode">'+
    '<option value="default" selected="selected">default</option>'+
      '<option value="vim">vim</option>'+
      '<option value="emacs">emacs</option>'+
  '</select>'+
  '</span>';//+

//  '<!--'+
//  '<input type="checkbox" value="readonly" id="ro" onchange="rwchange()" style="float:right" />'+
//  '-->'+
//  '</div>';

    //'<form>'+
    //'</form>';



//create new wrapper
var container = document.createElement('div');
container['style']["border"] = "1px solid #BBBBBB";
container['style']["background-color"] = "#EEEEEE";
container.innerHTML = src;

//attach
//txtarea.parentNode.replaceChild( container, txtarea );
//container.appendChild(txtarea);
txtarea.parentNode.insertBefore( container, txtarea );



//var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
var editor = CodeMirror.fromTextArea( txtarea, {
    lineNumbers : true,
    lineWrapping: true,
    onCursorActivity: function() {
        editor.matchHighlight("CodeMirror-matchhighlight");
        editor.setLineClass(hlLine, null, null);
        hlLine = editor.setLineClass(editor.getCursor().line, null, "activeline");
    },
    onGutterClick: function(cm, n) {
        var info = cm.lineInfo(n);
        if (info.markerText){
            cm.clearMarker(n);
        }else{
            //cm.setMarker( n, '<span style="color: #4a5fff; font-size: 16px;">&raquo;</span> %N%' );
            cm.setMarker(n, "%N%", "markedline");
        }
    }
});

CodeMirror.modeURL = 'codemirror/mode/%N/%N.js';
var hlLine = editor.setLineClass(0, "activeline");


document.getElementById('linenum').onkeyup = lnkeyup;
document.getElementById('linenum').onchange = lnchange;
document.getElementById('mode').onchange = change;

document.getElementById('undobtn').onclick = editor.undo;
document.getElementById('redobtn').onclick = editor.redo;
document.getElementById('formatbtn').onclick = formatall;
document.getElementById('keymode').onchange = keyschange;




var mimeLibs = {
"text/plain":{"libs":[],"mode":""},
"text/javascript":{"libs":["javascript"]},
"application/json":{"libs":["javascript"]},
"text/html":{"libs":["xml","javascript","css","htmlmixed"]},
"text/css":{"libs":["css"]},
"text/x-less":{"libs":["less"]},
"text/x-python":{"libs":["python"]},
"text/x-python":{"libs":["python"],"mode":"{name: \"python\",version: 2}"},
"text/x-php":{"libs":["clike","php"]},
"text/x-perl":{"libs":["perl"]},
"text/x-lua":{"libs":["lua"]},
"text/x-ruby":{"libs":["ruby"]},
"text/vbscript":{"libs":["vbscript"]},
"text/x-coffeescript":{"libs":["coffeescript"]},
"application/x-httpd-php":{"libs":["clike","php","xml","javascript","css"]},
"application/x-jsp":{"libs":["xml","javascript","css","htmlmixed","htmlembedded"]},
"application/x-aspx":{"libs":["xml","javascript","css","htmlmixed","htmlembedded"]},
"text/x-java":{"libs":["clike"]},
"text/x-groovy":{"libs":["groovy"]},
"text/x-scala":{"libs":["clike"]},
"text/x-erlang":{"libs":["erlang"]},
"text/x-csharp":{"libs":["clike"]},
"text/x-c++src":{"libs":["clike"]},
"text/x-csrc":{"libs":["clike"]},
"text/x-mysql":{"libs":["mysql"]},
"text/x-plsql":{"libs":["plsql"]},
"application/xml":{"libs":["xml"]},
"text/x-rst":{"libs":["rst"]},
"text/x-markdown":{"libs":["xml","markdown"]},
"text/x-tiddlywiki":{"libs":["tiddlywiki"],"styles":["tiddlywiki"]},
"tiki":{"libs":["tiki"],"styles":["tiki"]},
"text/x-stex":{"libs":["stex"]},
"text/x-yaml":{"libs":["yaml"]},
"text/x-smarty":{"libs":["smarty"]},
"text/velocity":{"libs":["velocity"]},
"application/xquery":{"libs":["xquery"]},
"jinja2":{"libs":["jinja2"],"mode":'{name: "jinja2", htmlMode: true}'},
"text/x-sh":{"libs":["shell"]},
"text/x-diff":{"libs":["diff"]},
"text/x-ini":{"libs":["properties"]},
"text/x-rpm-changes":{"libs":["changes"]},
"text/x-rpm-spec":{"libs":["spec"],"styles":["spec"]},
"text/x-clojure":{"libs":["clojure"]},
"text/x-haskell":{"libs":["haskell"]},
"text/x-pascal":{"libs":["pascal"]},
"text/x-scheme":{"libs":["scheme"]},
"text/x-stsrc":{"libs":["smalltalk"]},
"text/x-go":{"libs":["go"]},
"text/x-rsrc":{"libs":["r"]},
"text/x-ecl":{"libs":["ecl"]},
"text/x-rustsrc":{"libs":["rust"]},
"text/x-vb":{"libs":["vb"]},
"text/x-haxe":{"libs":["haxe"]},
"application/sieve":{"libs":["sieve"]}
};

function styleElem( file ){
   var elem = document.createElement("link");
   elem.setAttribute("type", "text/css");
   elem.setAttribute("rel", "stylesheet");
   elem.setAttribute("href", file);
   return elem;
}

var modeInput = document.getElementById("mode");
function change() {
	var selectedMode = modeInput.value;
//alertselectedMode();

   if( mimeLibs.hasOwnProperty( selectedMode ) ){
   var mh = mimeLibs[selectedMode];
   var libs = mh["libs"];

   for( var i =0; i< libs.length; i++ ){
   	CodeMirror.autoLoadMode(editor, libs[i]);
   }
   
   if( mh.hasOwnProperty( "styles" ) ){
   var styles = mh["styles"];
   
   for( var j =0; j< styles.length; j++ ){
   		document.head.appendChild( styleElem( "codemirror/mode/"+styles[j]+"/"+styles[j]+".css" ) );
   }   
   }
   
   if( mh.hasOwnProperty("mode") ){
   selectedMode = mh["mode"];
   }
   editor.setOption("mode", selectedMode);
   
   //TODO: load styles
   }
}

function rwchange() {
  var ro = document.getElementById("ro").checked ? "nocursor":false;
//alert(ro);
   editor.setOption("readOnly", ro );
}

function keyschange() {
  var k = document.getElementById("keymode").value;
//alert(k);
   editor.setOption("keyMap", k );
}

function lnchange() {
  var n = parseInt( document.getElementById("linenum").value );
//alert(k);
   editor.setOption("firstLineNumber", n );
}
function lnkeyup() {
    //onkeyup="this.value=this.value.replace('+"/[^\d]/,'')"+'"
    document.getElementById("linenum").value = document.getElementById("linenum").value.replace(/[^\d]/,'');
}


function formatall() {
  //CodeMirror.commands["selectAll"](editor);
  editor.setSelection({line: 0, ch: 0}, {line: editor.lineCount() - 1});
  try{
  editor.autoFormatRange( editor.getCursor(true), editor.getCursor(false) );
  }catch(e){}
  editor.setSelection({line: 0, ch: 0}, {line: 0, ch: 0});
}



    anyeditor.getCode = null;
    anyeditor.setCode = null;
    anyeditor.getLineNum = null;
    anyeditor.setLineNum = null;
    anyeditor.getLineMarkers = null;
    anyeditor.setLineMarkers = null;
    anyeditor.getLang = null;
    anyeditor.setLang = null;

    anyeditor.editor = editor;

    return anyeditor;
};  //end CodeMirrorAnyEditor
